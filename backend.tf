terraform {
  backend "azurerm" {
    storage_account_name = "dev-state"
    container_name       = "dev-test"
    key                  = "test-1"
    environment          = "public"
  }
}
